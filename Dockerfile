FROM ubuntu:latest

RUN apt-get update && apt-get install -y dpkg

COPY ./build/bin/*.deb /tmp/

RUN dpkg -i /tmp/*.deb || true

CMD ["bash"]
