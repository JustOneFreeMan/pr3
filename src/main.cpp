#include <iostream>
#include "factorial.h"
#include <cstdlib>

using namespace std;

int main(int argc, char* argv[]) {
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " <number>\n";
        return 1;
    }

    int n = atoi(argv[1]);

    if (n <= 0) {
        cerr << "Error: Invalid input. Please enter a positive integer.\n";
        return 1;
    }

    unsigned long long result = factorial(n);
    cout << "Factorial of " << n << " is " << result << endl;

    return 0;
}
