#include <gtest/gtest.h>

#include "factorial.h"

TEST(FactorialTest, PositiveNumber) {
EXPECT_EQ(factorial(0), 1);
EXPECT_EQ(factorial(1), 1);
EXPECT_EQ(factorial(5), 120);
EXPECT_EQ(factorial(10), 3628800);
EXPECT_EQ(factorial(20), 2432902008176640000);
}

TEST(FactorialTest, NegativeNumber) {
EXPECT_EQ(factorial(-5), 0);
EXPECT_EQ(factorial(-10), 0);
EXPECT_EQ(factorial(-20), 0);
}

TEST(FactorialTest, LargeNumber) {
EXPECT_EQ(factorial(21), 0);
EXPECT_EQ(factorial(100), 0);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
