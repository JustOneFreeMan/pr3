#include <iostream>
#include "factorial.h"

using namespace std;


unsigned long long factorial(int n) {
    if (n < 0) {
        cerr << "Error: Factorial is defined only for non-negative integers.\n";
        return 0;
    }

    if (n > 20) {
        cerr << "Error: Number is too large to calculate factorial.\n";
        return 0;
    }

    unsigned long long result = 1;
    for (int i = 1; i <= n; ++i) {
        result *= i;
    }

    return result;
}